package me.code;


import net.md_5.bungee.api.plugin.Plugin;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class BungeeMain extends Plugin {

    @Override
    public void onEnable(){
        getProxy().getPluginManager().registerCommand(this, new getPointCommand());
        getProxy().getPluginManager().registerCommand(this, new addPoint());
    }

}
