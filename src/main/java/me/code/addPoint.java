package me.code;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class addPoint extends Command{
    private static final String USER_AGENT = "Mozilla/5.0";

    public addPoint() {
        super("addPoint");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) commandSender;
        if(args.length == 2){
            if (commandSender != null){
                try {
                    sendAddPoint(Integer.parseInt(args[0]), args[1]);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }else{
            player.sendMessage(new TextComponent(ChatColor.GRAY + "Precise points and color"));
        }
    }

    private static void sendAddPoint(int point, String color) throws IOException {
        String url = "http://40.89.144.74:8080/api/v1/addPoint/" + color + "/" + point;
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        //Send post request
        Isend.send(con);
    }
}
