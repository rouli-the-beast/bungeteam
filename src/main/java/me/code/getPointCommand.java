package me.code;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;

public class getPointCommand extends Command {

    private static final String USER_AGENT = "Mozilla/5.0";

    private static final String GET_URL = "http://40.89.144.74:8080/api/v1/allTeams";

    public getPointCommand() {
        super("getPoint");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer player = (ProxiedPlayer) commandSender;
        if(args.length == 1){
            if (commandSender != null){
                Object points = null;
                JSONParser parser = new JSONParser();
                try {
                    Object obj = parser.parse(sendGET());

                    JSONObject jsonObject = (JSONObject) obj;

                    JSONArray teams = (JSONArray) jsonObject.get("teams");

                    Iterator<Object> iterator = teams.iterator();
                    while (iterator.hasNext()) {
                        JSONObject obj2 = (JSONObject) iterator.next();
                        if(obj2.get("name").toString().equals(args[0])){
                            points = obj2.get("score");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                player.sendMessage(new TextComponent(ChatColor.GRAY + args[0] + " points is: " + ChatColor.GREEN + points));
            }
        }else{
            player.sendMessage(new TextComponent(ChatColor.GRAY + "Precise a unique color"));
        }
    }

    private static String sendGET() throws IOException {
        URL urlObj = new URL(GET_URL);
        HttpURLConnection con = (HttpURLConnection) urlObj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);
        int responseCode = con.getResponseCode();
        StringBuffer response = new StringBuffer();
        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
        } else {
            System.out.println("GET request not worked");
        }

        return response.toString();
    }
}
